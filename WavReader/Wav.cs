﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WavReader
{
    class Wav
    {
        public string ChunkID;
        public int ChunkSize;
        public float ChunkSizeMb;
        public string ChunkFormat;

        public string SubchunkID;
        public int SubchunkSize;
        public int AudioFormat;
        public int NumberOfChannels;
        public int SamplesPerSecond;
        public int BytesPerSecond;
        public int BlockAlign;
        public int BitsPerSample;

        public string Subchunk2ID;
        public int Subchunk2Size;

        public double[] Sample;

        public void ReadHeader(string FileName)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(FileName, FileMode.Open)))
            {
                ChunkID = new string(reader.ReadChars(4));
                ChunkSize = reader.ReadInt32();
                ChunkSizeMb = (ChunkSize / 1024f) / 1024f;
                ChunkFormat = new string(reader.ReadChars(4));
                SubchunkID = new string(reader.ReadChars(4));
                SubchunkSize = reader.ReadInt32();
                AudioFormat = reader.ReadInt16();
                NumberOfChannels = reader.ReadInt16();
                SamplesPerSecond = reader.ReadInt32();
                BytesPerSecond = reader.ReadInt32();
                BlockAlign = reader.ReadInt16();
                BitsPerSample = reader.ReadInt16();
                Subchunk2ID = new string(reader.ReadChars(4));
                Subchunk2Size = reader.ReadInt32();
                Sample = new double[1024 * BitsPerSample / 8];


                for (int i = 0; i < Sample.Length; i++)
                {
                    switch (BitsPerSample)
                    {
                        case 8:
                            Sample[i] = reader.ReadInt16();
                            break;
                        case 16:
                            Sample[i] = reader.ReadInt16();
                            break;
                        case 32:
                            Sample[i] = reader.ReadInt32();
                            break;
                    }
                }
            }

        }

        public void PrintHeader()
        {
            Console.WriteLine("ChunkID: " + ChunkID);
            Console.WriteLine("ChunkSize: " + ChunkSize);
            Console.WriteLine("ChunkSizeMb: " + ChunkSizeMb);
            Console.WriteLine("ChunkFormat: " + ChunkFormat);
            Console.WriteLine("SubchunkID: " + SubchunkID);
            Console.WriteLine("SubchunkSize: " + SubchunkSize);
            Console.WriteLine("AudioFormat: " + AudioFormat);
            Console.WriteLine("NumberOfChannels: " + NumberOfChannels);
            Console.WriteLine("SamplesPerSecond: " + SamplesPerSecond);
            Console.WriteLine("BytesPerSecond: " + BytesPerSecond);
            Console.WriteLine("BlockAlign:  " + BlockAlign);
            Console.WriteLine("BitsPerSample: " + BitsPerSample);
            Console.WriteLine("Subchunk2ID:  " + Subchunk2ID);
            Console.WriteLine("Subchunk2Size:  " + Subchunk2Size);

            Console.WriteLine("Sample: ");
            for (int i = 0; i < Sample.Length; i++)
            {
                Console.WriteLine(i + ": " + Sample[i]);
            }
        }
    }


}
